import { Component } from '@angular/core';
import {TaskModel} from "./task/task.model";
import { CdkDragDrop, transferArrayItem } from '@angular/cdk/drag-drop';
import {MatDialog} from "@angular/material/dialog";
import {TaskDialogComponent, TaskDialogResult} from "./task-dialog/task-dialog.component";
import {AngularFirestore, AngularFirestoreCollection} from "@angular/fire/compat/firestore";
import {BehaviorSubject, Observable} from "rxjs";

// Wrap into a BehaviorSubject so that we don't create a new
// underlying array every time. This way the reorder method
// of the CDK drag & drop would work since it'll operate
// over the same array reference that we use to render the lists.
const getObservable = (collection: AngularFirestoreCollection<TaskModel>) => {
  const subject = new BehaviorSubject<TaskModel[]>([]);
  collection.valueChanges({ idField: 'id' }).subscribe((val: TaskModel[]) => {
    subject.next(val);
  });
  return subject;
};


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  doneList: any;
   /*
   todo: TaskModel[] = [
    {
      title: 'Buy milk',
      description: 'Go to the store and buy milk'
    },
    {
      title: 'Create a Kanban app',
      description: 'Using Firebase and Angular create a Kanban app!'
    }
  ];
  inProgress: TaskModel[] = [];
  done: TaskModel[] = [];
  */

  // @ts-ignore
  // todo = this.store.collection('todo').valueChanges({ idField: 'id' }) as Observable<Task[]>;
  // @ts-ignore
  // inProgress = this.store.collection('inProgress').valueChanges({ idField: 'id' }) as Observable<Task[]>;
  // @ts-ignore
  // done = this.store.collection('done').valueChanges({ idField: 'id' }) as Observable<Task[]>;

  todo = getObservable(this.store.collection('todo'));
  inProgress = getObservable(this.store.collection('inProgress'));
  done = getObservable(this.store.collection('done'));

  constructor(private dialog: MatDialog, private store: AngularFirestore) {
  }



  /*
  drop(event: CdkDragDrop<TaskModel[]>): void {
    if (event.previousContainer === event.container) {
      return;
    }
    if (!event.container.data || !event.previousContainer.data) {
      return;
    }
    transferArrayItem(
      event.previousContainer.data,
      event.container.data,
      event.previousIndex,
      event.currentIndex
    );
  }
*/


  drop(event: CdkDragDrop<TaskModel[]|null>): void {
    if (event.previousContainer === event.container) {
      return;
    }
    if (!event.previousContainer.data || !event.container.data) {
      return;
    }
    const item = event.previousContainer.data[event.previousIndex];
    this.store.firestore.runTransaction(() => {
      const promise = Promise.all([
        this.store.collection(event.previousContainer.id).doc(item.id).delete(),
        this.store.collection(event.container.id).add(item),
      ]);
      return promise;
    });
    transferArrayItem(
      event.previousContainer.data,
      event.container.data,
      event.previousIndex,
      event.currentIndex
    );
  }

  editTask(list: 'done' | 'todo' | 'inProgress', task: TaskModel): void {
    const dialogRef = this.dialog.open(TaskDialogComponent, {
      width: '270px',
      data: {
        task,
        enableDelete: true,
      },
    });
    dialogRef.afterClosed().subscribe((result: TaskDialogResult) => {
      if (result.delete) {
        this.store.collection(list).doc(task.id).delete();
      } else {
        this.store.collection(list).doc(task.id).update(task);
      }
    });
  }

  newTask(): void {
    const dialogRef = this.dialog.open(TaskDialogComponent, {
      width: '270px',
      data: {
        enableDelete: false,
        task: {},
      },
    });
    dialogRef
      .afterClosed()
      .subscribe((result: TaskDialogResult) => this.store.collection('todo').add(result.task));
  }

}
