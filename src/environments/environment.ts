// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    projectId: 'todo-app-c109c',
    appId: '1:278112735210:web:fb5612ea94a464d7012c93',
    storageBucket: 'todo-app-c109c.appspot.com',
    locationId: 'europe-west',
    apiKey: 'AIzaSyB-taNCxl2zrDJfDkj9_RUZZObzuobMzVg',
    authDomain: 'todo-app-c109c.firebaseapp.com',
    messagingSenderId: '278112735210',
    measurementId: 'G-M5VZ7S5JCB',
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
